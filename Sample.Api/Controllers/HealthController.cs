﻿using System.Web.Http;

namespace Sample.Api.Controllers
{
    public class HealthController : ApiController
    {
        [Route("api/health")]
        public string Get()
        {
            return "Up and at 'em, Atom Ant!";
        }
    }
}